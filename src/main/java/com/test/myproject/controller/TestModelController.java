package com.test.myproject.controller;

import com.test.myproject.model.TestModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;

@Controller
public class TestModelController {

    @RequestMapping("/getModel")
    public ResponseEntity<TestModel> getModel(@RequestParam(value = "id") BigInteger id, @RequestParam(value = "name") String name){
        return new ResponseEntity<>(new TestModel(id, name), HttpStatus.OK);
    }

}
